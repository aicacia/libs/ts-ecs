# ts-ecs

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-ecs/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/ecs)](https://www.npmjs.com/package/@aicacia/ecs)
[![pipelines](https://gitlab.com/aicacia/libs/ts-ecs/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-ecs/-/pipelines)

an entity component system
